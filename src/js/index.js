import '../css/style.css';
import '../images/arrow.png';
import '../images/allgood.png';

//Для нумерации шагов
const articleNumbers = document.getElementsByClassName('step__number');
//Для скролла
const stepSection = document.getElementsByClassName('step')[0];
const stepList = document.getElementsByClassName('step__container');
let currentStep = 0;
//Для перехода на следующий шаг
const formsSelect = document.getElementsByClassName('form-select');
//Для текстовой формы
const btnSubmit = document.querySelector("[type='submit']");
const inputName = document.getElementById('fullname');
const inputNumber = document.getElementById('number');

//Нумерация шагов
function numerateSteps() {
    for (let i = 0; i < articleNumbers.length; i++) {
        articleNumbers[i].innerHTML = 'Шаг ' + `${i + 1}/9`;
    }
}

//Скролл
function defineDirection(e) {
    if (e.target.closest('.input_radio')) {
        stepList[currentStep].querySelector('.btn-next').disabled = false;
        scrollSection();
    }
    if (e.target.closest('.btn-next')) {
        scrollSection();
    }
    if (e.target.closest('.btn-prev')) {
        scrollSection(-1);
    }
}

//Функция скролла
function scrollSection(k = 1) {
    currentStep += k;
    stepSection.scrollTo(currentStep * (stepSection.clientWidth + 16), 0);
}

//При выборе опции
function nextFromSelect(e) {
    e.currentTarget.closest('.form-select').querySelector('.btn-next').disabled = e.currentTarget.value === '' ? true : false;
}

//Заглушка с регулярными выражениями
function dummyFunction(e) {
    e.preventDefault();
    const [input1, msg1] = [...checkInputName()];
    if (input1) {
        const [input2, msg2] = [...checkInputNumber()];
        if (input2) {
            scrollSection();
        }
        else {
            noteError(inputNumber, msg2);
        }
    }
    else {
        noteError(inputName, msg1);
    }
}

//Вывод ошибок при заполнении
function noteError(elem, msg) {
    btnSubmit.disabled = true;
    const saveInput = elem.value;
    elem.value = msg;
    elem.style.cssText = `
        color: red;
        background-color: rgb(236, 212, 212);
        border: 1px solid darkred;
    `;
    setTimeout(() => {
        elem.removeAttribute('style');
        elem.value = saveInput;
        btnSubmit.disabled = false;
    }, 2000);
}

function checkInputName() {
    if (/\d/.test(inputName.value)) { // исключаем цифры
        return [false, 'Имя: В имени не может быть цифр -'];
    }
    if (/[^\w\-а-я ]/i.test(inputName.value)) { //исключаем знаки препинания (кроме - и пробела), добавляем кириллицу
        return [false, 'Имя: В имени не может быть знаков препинания -'];
    }
    if (inputName.value === '') {
        return [false, 'Это поле не может быть пустым -'];
    }
    return [true, ''];
}

function checkInputNumber() {
    if (inputNumber.value === '') {
        return [false, 'Это поле не может быть пустым -'];
    }
    if (/[^\d]/.test(inputNumber.value)) { // исключаем всё, кроме цифр
        return [false, 'Телефон: только цифры -'];
    }
    if (/^[^\D78]/.test(inputNumber.value)) { // номер начинается только на 7 и 8
        return [false, 'Телефон: 7/8ХХХХХХХХХХ формата -'];
    }
    if (inputNumber.value.length !== 11) // проверка длины номера
        return [false, 'Телефон: номер состоит из 11 цифр -']
    return [true, ''];
}

window.onload = () => {
    numerateSteps();
    stepSection.addEventListener('click', defineDirection);
    for (let stepSelect of formsSelect) {
        stepSelect.getElementsByTagName('select')[0].addEventListener('change', nextFromSelect);
    }
    btnSubmit.parentElement.addEventListener('submit', dummyFunction);
    window.addEventListener('resize', function () {
        stepSection.scrollTo({
            left: currentStep * (stepSection.clientWidth + 16),
            behavior: 'instant'
        });
    });
}
